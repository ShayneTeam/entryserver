﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntryServer
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("ESC: Quit");

            EntryServer server = new EntryServer();

            try
            {
                server.Start();
                Console.WriteLine("Entry Server start ok");
                
                while (true)
                {
                    if (Console.KeyAvailable && Console.ReadKey(true).Key == ConsoleKey.Escape)
                    {
                        break;
                    }

                    System.Threading.Thread.Sleep(1000);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("{0}", e.Message.ToString());
            }
            finally
            {
                server.Dispose();
            }
        }
    }
}
