﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.CSharp.RuntimeBinder;
using System.Net;
using System.Net.Sockets;
using Nettention.Proud;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using GunnersBattleCommon;
using CouchbaseShared;
using System.Diagnostics;

namespace EntryServer
{
    public class RemoteUser
    {
        public HostID hostId = HostID.HostID_None;
        public HostID roomId = HostID.HostID_None;
        public string userId = "";
        public Stopwatch stopwatch = new Stopwatch();
    }

    partial class EntryServer
    {
        NetServer _netServer = new NetServer();

        ThreadPool _networkerThreadPool = new ThreadPool(8);
        ThreadPool _userworkerThreadPool = new ThreadPool(8);

        EntryS2C.Proxy _S2CProxy = new EntryS2C.Proxy();
        EntryC2S.Stub _C2SStub = new EntryC2S.Stub();

        System.Threading.Timer _refreshEntryStateTimerReference;

        string _currentServerGuid = System.Guid.NewGuid().ToString();
        string _currentServerPublicIP = new System.Net.WebClient().DownloadString("https://api.ipify.org/");

        // public ---------------------------------------------------------------------------------------------------------------------
        public EntryServer()
        {
            InitNetServer();

            InitRMI();
        }

        public void Start()
        {
            StartServerParameter sp = new StartServerParameter();
            sp.protocolVersion = new Nettention.Proud.Guid(GunnersBattleCommon.Vars.entryServerProtocolVersion);
            sp.tcpPorts = new IntArray();
#if DEBUG
            sp.tcpPorts.Add(10000);
#else
            sp.tcpPorts.Add(40000);
#endif

            // 게임 서버의 로드밸런싱을 위해 L4 스위치 뒤에 서버 호스트들을 설치하는 경우, 규칙을 L4 스위치에 적용을 해야함
            // ProudNet은 각 클라이언트에 대해 1개의 TCP 연결과 1개의 가상 UDP 연결을 유지하기 때문 
            // 따라서 같은 IP로부터 들어오는 클라이언트 연결은 언제나 같은 L4 스위치 뒤의 서버로 매핑되어야 함 
            // 만약 이러한 설정이 없으면 ProudNet의 UDP 통신과 직접 P2P 통신 기능이 정상적으로 작동하지 못함
            // 공유기 또는 L4 스위치 뒤에 있는 서버는 시작시 m_serverAddrAtClient를 설정해야 함
            //sp.serverAddrAtClient = GunnersBattleCommon.Vars.entryServerIP;
            sp.serverAddrAtClient = _currentServerPublicIP;

            Console.WriteLine("Public IP : {0}", _currentServerPublicIP);

            //sp.localNicAddr = "127.0.0.1";
            sp.SetExternalNetWorkerThreadPool(_networkerThreadPool);
            sp.SetExternalUserWorkerThreadPool(_userworkerThreadPool);
            //sp.allowServerAsP2PGroupMember = true;

            _netServer.Start(sp);


            // DB에 엔트리 현황 기록을 위한 타이머 
            var findConfig = CouchbaseManager.Instance.FindConfig("onlyServerKnown");
            if (findConfig.Success)
            {
                JObject onlyServerKnown = JObject.FromObject(findConfig.Value);

                int START_WAIT_TIME_MS = 0 * 1000;
                int PERIOD_TIME_MS = (int)onlyServerKnown["refreshEntryStateTimeSec"] * 1000;
                System.Threading.TimerCallback timerDelegate = new System.Threading.TimerCallback(RefreshEntryStateToDB);
                _refreshEntryStateTimerReference = new System.Threading.Timer(timerDelegate, this, START_WAIT_TIME_MS, PERIOD_TIME_MS);

                CouchbaseManager.Instance.UpsertEntry(_currentServerGuid, _currentServerPublicIP, _netServer.GetClientHostIDs().Length);
            }
        }

        public void Dispose()
        {
            _netServer.Dispose();

            _refreshEntryStateTimerReference.Dispose();
        }

        // private --------------------------------------------------------------------------------------------------------------------
        void RefreshEntryStateToDB(object obj)
        {
            CouchbaseManager.Instance.UpdateEntry(_currentServerGuid, _netServer.GetClientHostIDs().Length);

            RefreshEntryServerDB();

            //Console.WriteLine("Refresh Current Entry State");
        }

        public static void RefreshEntryServerDB()
        {
            var findConfig = CouchbaseManager.Instance.FindConfig("onlyServerKnown");
            if (findConfig.Success)
            {
                // 서버는 1회용처럼 언제든 죽을 수 있기 때문에 갱신 주기가 너무 딜레이될 시 서버가 죽은걸로 판정하고 현황에서 삭제함
                JObject onlyServerKnown = JObject.FromObject(findConfig.Value);
                int removeWaitingEntryStateTimeSec = (int)onlyServerKnown["removeWaitingEntryStateTimeSec"];

                // DB에서 엔트리 현황 리스트 가져오기 
                var getEntryListResult = CouchbaseManager.Instance.GetEntryList();
                if (getEntryListResult.Success)
                {
                    JObject resultRow = JObject.FromObject(getEntryListResult.Rows[0]);
                    JArray entryList = JArray.FromObject(resultRow["entryList"]);
                    foreach (JObject entry in entryList.Children<JObject>())
                    {
                        string id = (string)entry["id"];
                        int userCount = (int)entry["userCount"];
                        string ip = (string)entry["ip"];

                        // system.formatexception : the string was not recognized as a valid datetime
                        // AWS EC2는 영어 기반이기 때문에 한글기반 윈도우랑 form이 차이가 나서 DateTime.Parse할 떄 예외가 던져진다
                        // 영어 한글 둘다 쓸 수 있는 공통 형식을 만들어줘야겠다 
                        DateTime refreshedTime = DateTime.ParseExact((string)entry["refreshedTime"], SharedProject.Common.DateTimeForm, System.Globalization.CultureInfo.GetCultureInfo("ko-KR"));

                        // 이 DateTime.Now 하는 부분도 주의해야하는게 Parse한 refreshedTime이 자기 문화권이 아니라면
                        // 아마 UTC 기준으로 할 것이기 때문에 몇시간씩 차이가 날 것이다 
                        // 더 앞 시간이 될 수도 있고, 더 뒷 시간이 될 수도 있다
                        var interval = DateTime.Now - refreshedTime;

                        // 갱신 주기가 너무 오래됬다면 서버가 죽은걸로 판정하고 현황에서 삭제시킴
                        if (interval.TotalSeconds > removeWaitingEntryStateTimeSec)
                        {
                            CouchbaseManager.Instance.RemoveEntry(id);

                            Console.WriteLine("Remove Entry State. removedEntryId : {0}", id);
                        }
                    }
                }
            }
        }

        void InitNetServer()
        {
            _netServer.AttachProxy(_S2CProxy);
            _netServer.AttachStub(_C2SStub);

            _netServer.ConnectionRequestHandler = (AddrPort clientAddr, ByteArray userDataFromClient, ByteArray reply) =>
            {
                reply = new ByteArray();
                reply.Clear();

                return true;
            };

            _netServer.ClientHackSuspectedHandler = (HostID clinetID, HackType hackType) =>
            {
                System.Threading.Monitor.Enter(this);

                // 강제 연결 끊기 
                _netServer.CloseConnection(clinetID);

                System.Threading.Monitor.Exit(this);
            };

            _netServer.ClientJoinHandler = (NetClientInfo clientInfo) =>
            {
                Console.WriteLine("OnClientJoin: {0}", clientInfo.hostID);
            };

            _netServer.ClientLeaveHandler = (NetClientInfo clientInfo, ErrorInfo errorInfo, ByteArray comment) =>
            {
                Console.WriteLine("OnClientLeave: {0}", clientInfo.hostID);
            };

            _netServer.ErrorHandler = (ErrorInfo errorInfo) =>
            {
                Console.WriteLine("OnError: {0}", errorInfo.ToString());
            };

            _netServer.WarningHandler = (ErrorInfo errorInfo) =>
            {
                Console.WriteLine("OnWarning: {0}", errorInfo.ToString());
            };

            _netServer.ExceptionHandler = (Exception e) =>
            {
                Console.WriteLine("OnException: {0}", e.Message.ToString());
            };

            _netServer.InformationHandler = (ErrorInfo errorInfo) =>
            {
                Console.WriteLine("OnInformation: {0}", errorInfo.ToString());
            };

            _netServer.NoRmiProcessedHandler = (RmiID rmiID) =>
            {
                Console.WriteLine("OnNoRmiProcessed: {0}", rmiID);
            };

            _netServer.TickHandler = (object context) =>
            {

            };

            _netServer.UserWorkerThreadBeginHandler = () =>
            {
            };

            _netServer.UserWorkerThreadEndHandler = () =>
            {
            };
        }

        bool CheckAndroidHash(string androidHashFromUser)
        {
            var onlyServerKnown = CouchbaseManager.Instance.FindConfig("onlyServerKnown");
            JObject onlyServerKnownConfig = JObject.FromObject(onlyServerKnown.Value);

            string androidHash = (string)onlyServerKnownConfig["androidHash"];

            return androidHashFromUser == androidHash;
        }

        bool CheckVersion(string userVersion)
        {
            var findconfig = CouchbaseManager.Instance.FindConfig("static");
            JObject config = JObject.FromObject(findconfig.Value);

            // 버전 체크 
            string clientVersion = (string)config["clientVersion"];
            Char delimiter = '.';
            String[] clientVersionSubstrings = clientVersion.Split(delimiter);

            String[] userVersionSubstrings = userVersion.Split(delimiter);


            // 강제 업데이트 하위 버전이라면 에러 
            bool checkMainVersion = Convert.ToInt16(userVersionSubstrings[0]) >= Convert.ToInt16(clientVersionSubstrings[0]);
            bool checkSubVersion = Convert.ToInt16(userVersionSubstrings[1]) >= Convert.ToInt16(clientVersionSubstrings[1]);
            bool checkFixVersion = Convert.ToInt16(userVersionSubstrings[2]) >= Convert.ToInt16(clientVersionSubstrings[2]);

            // 같은 버전일 때랑 상위버전일 떄랑 비교자체가 달라진다 
            if (checkMainVersion)
            {
                bool isSameMainVersion = Convert.ToInt16(userVersionSubstrings[0]) == Convert.ToInt16(clientVersionSubstrings[0]);
                if (isSameMainVersion)
                {
                    if (checkSubVersion)
                    {
                        bool isSameSubVersion = Convert.ToInt16(userVersionSubstrings[1]) == Convert.ToInt16(clientVersionSubstrings[1]);
                        if (isSameSubVersion)
                        {
                            if (checkFixVersion)
                            {

                            }
                            else
                            {
                                return false;
                            }
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            else
            {
                return false;
            }

            return true;
        }

        void InitRMI()
        {
            _C2SStub.SignIn = SignIn;            
            _C2SStub.SignUp = SignUp;
            _C2SStub.RequestLobbyList = RequestLobbyList;
        }
        
        bool SignIn(HostID remote, RmiContext rmiContext, string jsonParam)
        {
            /*
            서버는 타입에 맞게 authToken을 인증에 사용
            인증된 플랫폼 ID가 나오면 DB에서 그 플랫폼ID를 가지고 있는 유저 검색
            (검색을 어떻게 해야하지 view로 검색해야 하나, 좀 느릴수도 있겠는데)
            결과 NotifySignIn
            result : success, authFail, notFoundUser, newUser, blocked, checkingServer, alreadyJoined
            user : 유저 데이터
            */

            System.Threading.Monitor.Enter(this);

            try
            { 
                JObject param = JObject.Parse(jsonParam);
                Console.WriteLine("SignIn param : " + param);

                JObject result = new JObject();

                do
                {
                    var findconfig = CouchbaseManager.Instance.FindConfig("static");
                    JObject config = JObject.FromObject(findconfig.Value);

                    // 버전 체크 
                    string clientVersion = (string)config["clientVersion"];
                    string userVersion = (string)param["version"];

                    if (!CheckVersion(userVersion))
                    {
                        result["result"] = "notMachingVersion";
                        result["latestVersion"] = clientVersion;

                        break;
                    }

                    

                    JObject user = null;

                    string type = param["type"].ToObject<string>();
                    if (type == "google")
                    {
                        // 해시 체크
                        string androidHashFromUser = (string)param["hash"];
                        if (CheckAndroidHash(androidHashFromUser) == false)
                        {
                            result["result"] = "notMachingHash";

                            break;
                        }

                        string token = param["token"].ToObject<string>();
                        var findResult = CouchbaseManager.Instance.FindUserWithField("googleId", token);
                        if (findResult.Success == false)
                        {
                            result["result"] = findResult.Message;

                            break;
                        }

                        user = JObject.FromObject(findResult.Value);
                    }
                    else
                    {
                        result["result"] = "undefineType";

                        break;
                    }


                    // 마지막 접속 시간 갱신해서 DB에 기록
                    user["lastSignInTime"] = DateTime.Now.ToString();

                    CouchbaseManager.Instance.UpdateUser(user);


                    result["result"] = "success";
                    result["user"] = user;
                    result["config"] = findconfig.Value;
                }
                while (false);
            

                string jsonResult = JsonConvert.SerializeObject(result);
                Console.WriteLine("SignIn result : " + jsonResult);

                _S2CProxy.NotifySignIn(remote, RmiContext.FastEncryptedReliableSend, jsonResult);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            System.Threading.Monitor.Exit(this);

            return true;
        }

        bool SignUp(HostID remote, RmiContext rmiContext, string jsonParam) 
        {
            /*
            result: success, authFail, existUser, notFoundUser, newUser, blocked, checkingServer
            user : 유저 데이터
            */
            System.Threading.Monitor.Enter(this);

            try
            { 
                JObject param = JObject.Parse(jsonParam);
                Console.WriteLine("SignUp param : " + param);

                JObject result = new JObject();

                do
                {
                    var findconfig = CouchbaseManager.Instance.FindConfig("static");
                    JObject config = JObject.FromObject(findconfig.Value);

                    // 버전 체크 
                    string clientVersion = (string)config["clientVersion"];
                    string userVersion = (string)param["version"];

                    if (!CheckVersion(userVersion))
                    {
                        result["result"] = "notMachingVersion";
                        result["latestVersion"] = clientVersion;

                        break;
                    }


                    // 없는 유저인지 DB에서 닉네임 검색
                    string nickname = (string)param["nickname"];
                    var findResult = CouchbaseManager.Instance.FindUserWithField("nickname", nickname);
                    if (findResult.Success)
                    {
                        result["result"] = "existUser";

                        break;
                    }

                    // DB에 절대 없는 유저여야만 함
                    if (findResult.Message != "notFoundUser")
                    {
                        result["result"] = findResult.Message;

                        break;
                    }

                    JObject user = new JObject();

                    string type = (string)param["type"];
                    if (type == "google")
                    {
                        // 해시 체크
                        string androidHashFromUser = (string)param["hash"];
                        if (CheckAndroidHash(androidHashFromUser) == false)
                        {
                            result["result"] = "notMachingHash";

                            break;
                        }

                        user["googleId"] = param["token"].ToObject<string>();
                    }
                    else
                    {
                        result["result"] = "undefineType";

                        break;
                    }

                    // 기본 계정 세팅
                    user["id"] = System.Guid.NewGuid().ToString();
                    user["nickname"] = nickname;
                    user["gem"] = 0;
                    user["character"] = "FreeMan";
                    user["weapon"] = "A_N16";
                    user["kill"] = 0;
                    user["death"] = 0;
                    user["win"] = 0;
                    user["lose"] = 0;
                    user["draw"] = 0;
                    user["rank"] = "F";
                    user["playTimeSec"] = 0;
                    user["createTime"] = DateTime.Now.ToString();
                    user["lastSignInTime"] = DateTime.Now.ToString();

                    JObject inventory = new JObject();
                    inventory[GunnersBattleCommon.ProductType.Chacracter.ToString()] = new JObject();
                    inventory[GunnersBattleCommon.ProductType.Weapon.ToString()] = new JObject();
                    inventory[GunnersBattleCommon.ProductType.Etcetera.ToString()] = new JObject();

                    // 기본 캐릭터 & 기본 무기 인벤토리에 넣어주기 
                    inventory[GunnersBattleCommon.ProductType.Chacracter.ToString()]["FreeMan"] = new JObject();
                    inventory[GunnersBattleCommon.ProductType.Weapon.ToString()]["A_N16"] = new JObject();
                    inventory[GunnersBattleCommon.ProductType.Weapon.ToString()]["S_Winchester"] = new JObject();
                    inventory[GunnersBattleCommon.ProductType.Weapon.ToString()]["N_TRG"] = new JObject();

                    user["inventory"] = inventory;

                    // DB에 추가 
                    var addUserResult = CouchbaseManager.Instance.AddUser(user);
                    if (addUserResult.Success == false)
                    {
                        result["result"] = addUserResult.Message;

                        break;
                    }

                    result["result"] = "success";
                    result["user"] = user;
                    result["config"] = findconfig.Value;
                }
                while (false);


                string jsonResult = JsonConvert.SerializeObject(result);
                Console.WriteLine("SignUp result : " + jsonResult);

                _S2CProxy.NotifySignUp(remote, RmiContext.FastEncryptedReliableSend, jsonResult);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            System.Threading.Monitor.Exit(this);

            return true;
        }

        bool RequestLobbyList(HostID remote, RmiContext rmiContext, string jsonParam)
        {
            System.Threading.Monitor.Enter(this);

            try
            { 
                JObject param = JObject.Parse(jsonParam);
                Console.WriteLine("RequestLobby param : " + param);

                JObject result = new JObject();

                do
                {
                    // DB에서 로비리스트 읽어오기 
                    var getLobbyListResult = CouchbaseManager.Instance.GetLobbyList();
                    if (getLobbyListResult.Success == false)
                    {
                        result["result"] = getLobbyListResult.Message;

                        break;
                    }

                    JObject resultRow = JObject.FromObject(getLobbyListResult.Rows[0]);

                    result["result"] = "success";
                    result["lobbyList"] = resultRow["lobbyList"];
                }
                while (false);

                string jsonResult = JsonConvert.SerializeObject(result);
                Console.WriteLine("RequestLobby result : " + jsonResult);

                _S2CProxy.NotifyRequestLobbyList(remote, RmiContext.ReliableSend, jsonResult);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            System.Threading.Monitor.Exit(this);

            return true;
        }
    }
}
